'use strict'

// Version 0.0.0

/*
 * This library's intentions is to connect to a SQL database and apply either a change or an entire schema
 *
 * Process:
 * First, accumulate all files with that match *.sql
 * Then connect to the database.
 * if the schema exists, then examine the _ph_migrations table against files that match *.migration.sql assuming hte pattern of X_<desc>.migration.sql for the dile wher desc is a short meanigful name and X is a version number (do not use dates as dates are generated at creation and that precludes parallel work.
 * If the schem does not exist, then run all *.sql files in meaningful order
 *
 * ToDo:
 * pass in config path
 *
 *
 * .schema.sql // We should require that the schema be passed in the config
 *        if it does not exist, then we know to create it and that is a very simple operation
 *
 * .type.sql
 * .table.sl
 * .constraint.sql
 * .sproc.sql
 * .data.sql
 *
 */

// Core
const fs = require('fs')

// Modules
const { Pool } = require('pg')

// Libs
const fileHelper = require('../lib/fileHelper/fileHelper')

// Values
const EXP_SQL_FILE = /\.sql$/
const EXP_SCHEMA = /\.schema\.sql$/
const EXP_TABLE = /\.table\.sql$/

// const QUERY_

module.exports = {
  apply,
  check,
  reapply
}

/**
 * Analyzes the database and either mounts the current version of the schema as described in code or runs between the
 * current version and the current version described.
 */
async function apply() {
  // Get config
  const config = require('../../datastore/phae_db.config') // ToDo: us an external representation of the path
  if (!_validateConfig(config)) {
    console.log('bad config') // ToDo: use Phaerr
    return
  }

  // Gather all files
  const defFiles = fileHelper.accumulator(process.cwd() + '/' + config.db_definition_path)
    .filter((name) => {
      return EXP_SQL_FILE.test(name)
    })
  const db = await _connect(config)

  // ToDo: validate if the database already exists, then branch

  await _mount_db_def(db, defFiles)

  await _disconnect(db)
}

/**
 * Checks the current version loaded in the database against the version expected in based on code and migrations.
 */
function check () {
  console.log('operation s currently not supported.')
}

/**
 * Wipes the current database and applies the schema as defined in code
 */
function reapply () {
  console.log('operation s currently not supported.')
}

// Private Functions
async function _connect (config) {
  const client = new Pool({
    user: config.db_user,
    host: config.db_url,
    database: config.db_db,
    password: config.db_password,
    port: config.db_port
  })

  // await client.connect({
  //   user: config.db_user,
  //   host: config.db_url,
  //   database: config.db_schema,
  //   password: config.db_password,
  //   port: config.db_port
  // })

  return client
}

async function _createTrackingTable(db) {
  await db.query(``)
}

async function _disconnect(db) {
  await db.end()
}

async function _mount_db_def(db, fileNames) {
  const schemas = fileNames.filter((name) => EXP_SCHEMA.test(name))
  const tables = fileNames.filter((name) => EXP_TABLE.test(name))

  try {
    // Schema
    for (const schema of schemas) {
      const query_text = fs.readFileSync(schema, 'utf8')
      await db.query(query_text)
    }

    // Tables
    for (const table of tables) {
      const query_text = fs.readFileSync(table, 'utf8')
      console.log(table)
      await db.query(query_text)
    }

  } catch(err) {
    console.log(err.stack)

  }
}

const _validateConfig = (config) => {
  const props = new Set(Object.keys(config))
  if(!props.has('db_db')) return false
  if(!props.has('db_definition_path')) return false
  if(!props.has('db_password')) return false
  if(!props.has('db_port')) return false
  if(!props.has('db_url')) return false
  if(!props.has('db_user')) return false

  return true
}
