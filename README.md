# Phae DB Apply
An opinionated tool for spinning up and configuring Postgres SQL database projects.

## Usage

## Reasoning
Database development tools seem to incline towards either DBA centric tools that are not intended to operate with the same local development and deployment strategy as the code portion of a project, or they function by applying only deltas onto an existing database. The first scenario results in the database portion of a project having separate version control and release management tools while the second sacrifices having a full-code version of the schema as part of the project.

It is my opinion that database version and deployment control cannot be separated without resulting in windows of down time and no clear roll-back procedure should one artifact or the other prove to defective in some way. It is also my opinion that changes cannot be adequately reviewed with only cahnge statements available.

The idea behind this project is that both are valuable. To review a change, it is important to see the actual change command as well as the intended structure after the change.

## Expected File Structure
